local movelib = require("movelib")
local timber = require("timber")("dough")

--- Wheat farming program.
--- We assume that there's a 9x9 area of wheat
--- Also we assume that the turlle starts in front of a chest and facing the field

-- X is wheat
-- O is water
-- T is a turtle facing towards positive z
-- C is a chest
-- 
-- ^ positive z
-- |
-- 
-- X X X X X X X X X 
-- X X X X X X X X X 
-- X X X X X X X X X 
-- X X X X X X X X X 
-- X X X X O X X X X 
-- X X X X X X X X X 
-- X X X X X X X X X 
-- X X X X X X X X X 
-- T X X X X X X X X     -> positive x
-- C C C C

-- (imagine the thing above is a square)
-- We also define the bottom left corner of the first farm field to be (0,0)

-- A fully grown wheat crop drops 1 wheat and 1-4 seeds. Assuming a 9x9 area,
-- this is at most this is 6 (5.0625) slots of seeds and 2 (1.5) stacks of wheat.
-- So if we always have an empty inventory, we don't have to worry about running
-- out of storage space. We can safely push this to a 9x18 area.

local WARN_STORAGE_LEVEL = 64*6
-- Usually we want to have very few seeds on us (like 1 stack).
-- 160 seeds is enough to replant a 9x18 area (with 2 spaces being water)
local MIN_SEED_COUNT = 160

-- width is x, depth is z
local WIDTH = 18
local DEPTH = 9

-- This is the amount of floors
local LEVEL_COUNT = 4
local currentLevel = 0

function main()
    -- The next level of the farm we have to check.
    local nextLevel = 0

    while true do
        timber.trace(("We're at level %d"):format(currentLevel))
        checkSurroundings()
        checkInventory()

        prepareInventory()
        collapseInventory()
        selectWheat()

        moveToLevel(nextLevel)

        for x, z in coverArea(WIDTH, DEPTH) do
            -- This is where water is
            if x % 9 == 4 and z % 9 == 4 then
                -- This is water :3
            else
                checkWheat()
            end
        end

        -- We assume we're at x = 0, z = 17 facing negativeZ
        movelib.run(("R %dF R"):format(WIDTH-1))
        moveToLevel(0)
        nextLevel = (nextLevel + 1) % LEVEL_COUNT

        -- Wait some amoutn of time?
        sleep(30*60 / LEVEL_COUNT)
    end
end

function coverArea(width, depth)
    local justStarted = true -- The first step we don't move.
    local x = 0
    local z = 0
    local direction = "positiveZ"
    return function()
        -- If we just started we do nothing
        if justStarted then
            justStarted = false
            return x, z
        end

        -- Check if we're done
        if direction == "positiveZ" then
            if z == depth-1 then
                if x == width-1 then
                    -- We're done
                    return nil
                end

                movelib.run("RFR")
                direction = "negativeZ"
                x = x + 1
            else
                movelib.run("F")
                z = z + 1
            end
        else -- if direction == "negativeZ"
            if z == 0 then
                if x == width-1 then
                    -- We're done
                    return nil
                end

                movelib.run("LFL")
                direction = "positiveZ"
                x = x + 1
            else
                movelib.run("F")
                z = z - 1
            end
        end

        return x, z
    end
end

function assertBlock(has_block, data, name)
    if not has_block then
        -- Since data is nil, name is actually moved 1 argument to the left...
        -- Ugh. Lua is bad
        error(("During startup we expected %s but found no block"):format(data))
    elseif data.name ~= name then
        error(("During startup we expected %s but found %s"):format(name, data.name))
    end
end

function assertNoBlock(has_block, data)
    if has_block then
        error(("During startup we expected no block but found %s"):format(data.name))
    end
end

function checkSurroundings()
    -- assertBlock(turtle.inspectDown(), "minecraft:wheat")
    assertBlock(turtle.inspectUp(), "minecraft:farmland")
    assertNoBlock(turtle.inspect())
end

function checkChest(name)
    -- Check chest space
    local chest = peripheral.find("minecraft:chest")
    local taken = 0
    for slot, item in pairs(chest.list()) do
        if item.name ~= name then
            error(("%s chest contains %s"):format(name, item.name))
        end
        taken = taken + item.count
    end
    local availabe = chest.size()*64 - taken

    if availabe < WARN_STORAGE_LEVEL then
        timber.warn(("Low on %s storage. Only %d spaces left."):format(name, availabe)) 
    else
        timber.info(("%d spaces left for %s."):format(availabe, name))
    end

    return availabe, taken
end

-- This function doesn't guarantee that we get n items.
-- If the chest doesn't contain n items, if the chest has more than 1 item type, 
-- if the turtle doesn't have enough space, if the items don't stack to 64 or if
-- some other unforseen situation occurs this will fail.
function takeFromChest(n)
    for stacks = 1, math.floor(n/64) do
        turtle.suckDown(64)
    end
    turtle.suckDown(n%64)
end

-- This has similar guarantee to takeFromChest (none)
function putIntoChest(n, name)
    local nLeft = n

    for i = 1, 16 do
        local item = turtle.getItemDetail(i)
        if item and item.name == name then
            turtle.select(i)
            -- If there's only (f.ex.) 10 storage spaces left, and we try to drop 64,
            -- we will only drop 10.
            local d = math.min(nLeft, item.count)
            turtle.dropDown(d)
            nLeft = nLeft - d

            if nLeft == 0 then
                break
            end
        end
    end
end

-- How many of this item do we have in our turtle?
function countItem(name)
    local count = 0
    for i = 1, 16 do
        local item = turtle.getItemDetail(i)
        if item and item.name == name then
            count = count + item.count
        end
    end

    return count
end

-- We assume that we're at x=0 z=0 and facing positive z
function prepareInventory()
    movelib.run("B")

    -- Check wheat chest
    local wheatStorage = checkChest("minecraft:wheat")

    -- Insert wheat
    for i = 1, 16 do
        local item = turtle.getItemDetail(i)
        if item and item.name == "minecraft:wheat" then
            turtle.select(i)
            turtle.dropDown()
            wheatStorage = wheatStorage - item.count
            if wheatStorage <= 0 then
                timber.error("Out of wheat storage space")
                break
            end
        end
    end

    movelib.run("R2F")

    -- Check seed chest
    local seedSpaces, seedsInStorage = checkChest("minecraft:wheat_seeds")
    local seedsOnTurtle = countItem("minecraft:wheat_seeds")
    timber.trace(("%d seeds on me"):format(seedsOnTurtle))

    if seedsOnTurtle > MIN_SEED_COUNT then
        -- Insert seeds
        if seedSpaces < seedsOnTurtle - MIN_SEED_COUNT then
            -- TODO: Add getting rid of excess seeds
            timber.warn("Too many fucking seeds.")
        end
        putIntoChest(math.min(seedsOnTurtle - MIN_SEED_COUNT, seedSpaces), "minecraft:wheat_seeds")
    elseif seedsOnTurtle < MIN_SEED_COUNT then
        -- Take seeds
        if seedsInStorage < MIN_SEED_COUNT - seedsOnTurtle then
            timber.warn("Not enough seeds to fill the turtle to the desired level!")
            timber.warn(("Wanted %d but there's only %d seeds in storage"):format(MIN_SEED_COUNT - seedsOnTurtle, seedsInStorage))
        end
        takeFromChest(math.min(MIN_SEED_COUNT - seedsOnTurtle, seedsInStorage))
    end


    movelib.run("2BLF")
end

-- In order to not waste space, we will collect partial stacks into whole stacks
-- We assume the inventory hasn't been modified since checkInventory has run
function collapseInventory()
    -- For every item type, we take a pass.
    -- In every pass, we take items at the end and move them earlier
    
    function pass(name)
        local head = 1
        local tail = 16

        while true do
            local itemHead = nil
            while true do
                itemHead = turtle.getItemDetail(head)
                if not itemHead or itemHead.name ~= name then
                    -- This head is empty, so we try the next one
                    head = head + 1
                    if head == tail then
                        -- We're done with pass, since head and tail overlap
                        return
                    end
                else
                    -- Valid head! yippe
                    break
                end
            end

            local itemTail = nil
            while true do
                itemTail = turtle.getItemDetail(tail)
                if not itemTail or itemTail.name ~= name then
                    -- This tail is empty, so we try the next one
                    tail = tail - 1
                    if head == tail then
                        -- We're done with pass, since tail and tail overlap
                        return
                    end
                else
                    -- Valid tail! yippe
                    break
                end
            end

            local availabe = 64 - itemHead.count
            turtle.select(tail)
            if availabe >= itemTail.count then
                turtle.transferTo(head)
                tail = tail - 1
            else
                -- available < itemTail.count
                turtle.transferTo(head)
                head = head + 1
            end
            
            if head == tail then
                -- We're done with pass, since tail and tail overlap
                return
            end
        end
    end

    pass("minecraft:wheat_seeds")
    pass("minecraft:wheat")
end

function selectWheat()
    local firstEmpty = nil

    for i = 1,16 do
        local item = turtle.getItemDetail(i)

        if not firstEmpty and not item then
            firstEmpty = i
        end
        if item and item.name == "minecraft:wheat" then
            turtle.select(i)
            return
        end
    end

    if firstEmpty == nil then
        error("No place to put wheat.")
    end
    turtle.select(firstEmpty)
end

-- We check that our inventory only contains wheat and wheat seeds
function checkInventory()
    allowedItems = {
        "minecraft:wheat",
        "minecraft:wheat_seeds"
    }

    local valid = true

    for i = 1,16 do
        local item = turtle.getItemDetail(i)

        if item and not contains(allowedItems, item.name) then
            timber.error("Illegal item " .. item.name .. " found.")
            valid = false
        end
    end

    if not valid then
        error("Inventory polluted")
    end
end

function checkWheat()
    local present, block = turtle.inspectDown()
    if not present then
        plantSeed()
    elseif block.name == "minecraft:wheat" then
        if block.state.age == 7 then
            turtle.digDown()
            plantSeed()
        end
        -- Do nothing, wait for it to grow
    else
        timber.info("Block "..block.name.." found!")
    end
end

function plantSeed()
    for i = 1, 16 do
        local item = turtle.getItemDetail(i)
        if item and item.name == "minecraft:wheat_seeds" then
            turtle.select(i)
            if not turtle.placeDown() then
                -- We can't place seeds. Let's investigate why
                -- We already know the block below is us air, this is why we've called plantSeed
                -- So let's check what the block below it is (it should be tilled dirt)
                -- But before we check, let's try to till it.
                if turtle.digDown() then
                    -- We could till it! Let's try planting again.
                    if turtle.placeDown() then
                        -- Ok, we planted them succesfully
                        return
                    else
                        timber.error("Can't place seeds! Very weird since I just tilled the dirt.")
                        return
                    end
                else
                    -- We can't till the ground here. Let's investiage and report
                    movelib.run("D")
                    local present, block = turtle.inspectDown()
                    timber.error(("Couldn't plant seeds on top of %s"):format(present and block.name or "air"))
                    movelib.run("U")
                    return
                end
            end

            return
        end
    end

    timber.warning("No seeds :(")
end

-- This assumes that x, z = (0,0)
function moveToLevel(n)
    if n >= LEVEL_COUNT then
        error(("Can't go to level %d since there are only %d levels."):format(n, LEVEL_COUNT))
    end

    if n == currentLevel then
        return
    elseif n > currentLevel then
        movelib.run(("B %d(3U) F"):format(n-currentLevel))
    else -- if n < currentLevel then
        movelib.run(("B %d(3D) F"):format(currentLevel-n))
    end

    currentLevel = n
end

timber.startWithLogging(main)

--[[

Todo
 - [x] Make it move back lol
 - [x] Make it recognize untilled dirt
 - [x] Make it recognize destroyed dirt
 - [x] Make it recognize water
 - [x] Inventory management
 - [ ] Fuel checking? idk
 - [x] Put shit in chest
 - [x] Cover area doesn't return (0,0) even though it should
 - [ ] Skip putting stuff in chests if I have nothing to put in chests
 - [x] Multiple levels
 - [ ] Save position (and other state) so we can recover in the case of ungraceful shutdown
    - Maybe this requires GPS? I'm not sure if we can trust coordinates.
 - [ ] Fix wheat stacking issue
    - If the first slot is empty and the turtle harvests wheat it ends in the first slot
      but if the turtle selects the second slot (because that one has seeds) then it
 - [x] Make turtle take seeds if it has too few

]]--