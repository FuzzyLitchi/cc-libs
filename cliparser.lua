local cliparser = {}

--- Inspired by Rust's [clap](https://crates.io/crates/clap)
-- 
-- example:
-- local options = {
--     ignoreFuel = {
--         short = "f",
--         long = "ignore-fuel",
--         description = "Don't check fuel level before running actions.",
--         type = "flag",
--     },
--     ignoreMissing = {
--         short = "m",
--         long = "ignore-missing",
--         description = "Don't check if there's enough items before running actions.",
--         type = "flag",
--     },
--     INPUT = {
--         description = "The path for the turtle to take.",
--         type = "string",
--     },
-- }
--
-- local args = cliparser.parse({}, options)

-- TODO:
-- - [ ] Document "default"
-- - [ ] Test "default"
-- - [ ] Document "required"
-- - [ ] check default and required = true aren't set for the same value.

local pretty = require("cc.pretty")

function cliparser.parse(arguments, options)
    -- Check options
    checkName(options)
    checkNoDuplicates(options)
    checkValidType(options)
    checkDefaultIsRightType(options)

    local matched = {}

    local i = 1
    while i <= #arguments do
        local part = arguments[i]
        local name, option
        if part:sub(1, 2) == "--" then
            name, option = findLong(options, part:sub(3))
        elseif part:sub(1, 1) == "-" then
            name, option = findShort(options, part:sub(2))
        else
            assert(not matched["INPUT"]) -- FIXME: Better user errors
            -- THIS IS HORRIBLE
            name = "INPUT"
            option = options["INPUT"]
            i = i - 1
        end

        assert(option, "Couldn't match \"" .. part .. "\" to anything.") -- FIXME: Better user errors
        
        if option.type == "flag" then
            matched[name] = true
        else
            i = i + 1
            local data = arguments[i]
            assert(data, "Missing value after " .. name .. " flag.") -- FIXME: Better user errors
            matched[name] = convertToType(data, option.type)
        end

        i = i + 1
    end

    -- Check if we're missing non-optional values
    for name, option in pairs(options) do
        if option.required and not matched[name] then
            error("Missing required option \"" .. name .. "\".")
        end
    end

    -- Set default if we didn't match it
    for name, option in pairs(options) do
        if not matched[name] and option.default then
            matched[name] = options.default
        end
    end

    return matched
end

--- Finds the argument options that has the long name `long`
function findLong(options, long)
    for name, option in pairs(options) do
        if option.long == long then
            return name, option
        end
    end
    return nil
end

--- Finds the argument options that has the short name `short`
function findShort(options, short)
    for name, option in pairs(options) do
        if option.short == short then
            return name, option
        end
    end
    return nil
end

function convertToType(data, type)
    if type == "string" then
        return data
    elseif type == "int" then
        data = tonumber(data)
        assert(isInt(data), "Expected int got float.") -- FIXME: This is a bad error message
        return data
    else
        error("TODO: " .. type)
    end
end

-- Errors if there are duplicate short/long identifiers
function checkNoDuplicates(options)
    for i = 1, #options do
        for j = i + 1, #options do
            local a = options[i]
            local b = options[j]

            if a.short and b.short then
                assert(a.short ~= b.short, "Two or more options use the short value \"" .. a.short .. "\".")
            end
            if a.long and b.long then
                assert(a.long ~= b.long, "Two or more options use the long value \"" .. a.long .. "\".")
            end
        end
    end
end

-- Errors if an option doesn't have a short OR a long name.
function checkName(options)
    for name, option in pairs(options) do
        if not name == "INPUT" then
            assert(option.short or option.long, "Argument " .. name .. "doesn't have a short or long name.")
        end
    end
end

-- Errors if a default value has the wrong type
function checkDefaultIsRightType(options)
    for name, option in pairs(options) do
        -- Only check if it has a default value
        if option.default then
            if options.type == "int" then
                assert(isInt(option.default), "Default value \"" .. option.default .. "\" for " .. name .. " is not an int.")
            elseif options.type == "flag" then
                assert(option.default == nil) -- FIXME
            elseif option.type == "string" then
                assert(type(option.default) == "string") -- FIXME
            else
                error("Unimplemented")
            end
        end
    end
end

-- Errors if an option doesn't have a type
function checkValidType(options)
    for name, option in pairs(options) do
        assert(contains({"int", "string", "flag"}, option.type), "Option " .. name .. " does not have a valid type.")
    end
end

function isInt(n)
    return math.floor(n) == n
end

function contains(array, value)
    for _, element in ipairs(array) do
        if value == element then
            return true
        end
    end
    
    return false
end

return cliparser