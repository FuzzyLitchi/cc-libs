--- Example usage
--- local timber = require("timber")("Application Name")
--- timber.info("Hello, world!")
function init(programName)
    local timber = {}
    local LOG_CHANNEL = 108

    local file
    if false and fs.exists(programName .. ".log.lock") then
        print(programName .. ".log.lock exists. Not logging to file.")
    else
        -- fs.open(programName..".log.lock", "w")
        file = fs.open(programName .. ".log", "a")
    end

    function internal_log(log_message, level)
        local time = os.epoch("local") / 1000
        local message = tostring(log_message)
        local machine = os.getComputerLabel() or os.getComputerID()

        -- Write to network
        local modem = peripheral.find("modem")
        if modem then
            modem.transmit(LOG_CHANNEL, 0, textutils.serialize({
                time = time,
                programName = programName,
                message = message,
                machine = machine,
                level = level
            }))
        end

        local formatedTime = os.date("%H:%M:%S", time)

        -- Write to screen
        -- TODO: Implement color print (normal computers can't do color, so check for that)
        print(formatedTime .. ": " .. message)

        -- Write to file
        file.writeLine(formatedTime .. ": " .. message)
        file.flush()
    end

    function timber.error(message)
        internal_log(message, "error")
    end

    function timber.warning(message)
        internal_log(message, "warning")
    end

    function timber.info(message)
        internal_log(message, "info")
    end

    function timber.trace(message)
        internal_log(message, "trace")
    end

    --- call main using this function in order to log any errors that occur.
    function timber.startWithLogging(main)
        local status, err = pcall(main)

        if not status then
            timber.error(err)
        end
    end

    return timber
end

return init

-- Todo
-- - [ ] Make timber.trace(x, y) print both arguments