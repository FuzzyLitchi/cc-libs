# Computer Craft libraries

I'm making some tools to develop computer craft progams :3

## Apollo
Apollo is my development enviroment tool. It watches my directory and "uploades" the lua scripts to the turles/pocket computers. Rn it does this by just moving files, but in the future I want it to support some web api so it can work on remote servers.

## Movelib

A library to help describe movement paths that turtles can take and the operations you want to take on such paths. You can describe paths in a DSL, calculate the exact fuel usage and specify some moves as forced (turtle will break blocks/attack until it moves).

### Example
```lua
local movelib = require("movelib")

movelib.run("2F 2(LP RBP)")

-- Turtle moves 2 forward, and then repeats the following steps twice: Turn left, place, turn right, move back place.
-- This command replants a 2x2 square in front of the turtle.
```

## Timber

Timber is a logging library inspired by Rust's log crate and tracing crate. Currently it supports logging to the terminal and over a modem. The log can then be aggregated on a pocket computer. In the future I plan to rework much of this library to support a cleaner API, stack traces on crashes, crash dumps into files, and a better woodsman (log viewer) application that includes a lookback buffer.

## cliparser

This is supposed to be for parsing flags and stuff and automating a help message but i forgor 💀

## Applications

In order to develop good libraries, I am developing applications that require them. Mine and Tree (horrible names) mine and fell trees respectively. They utilize most of the functionality provided by the libraries. They're both kinda bad but the upside is that I have a lot of wood and ores lol

# TODO

- [ ] Add tests (a lot of movelib doesn't even need a turtle runtime, this could be done easy)
- [ ] fix findInActions (also wtf is up with that function lmao)
- [ ] write better docs on every function
- [ ] Make Apollo only copy select files to select computers (i.e. woodsman on pocket, mine and tree on turtles)
- [ ] Do the stuff I said I'd do in the README
- [x] Add flag to take the inverse path to `move`
- [ ] When you place blocks using move it should use the one currently selected, and when it runs out, it should find more of them :)
- [x] Add logging levels
- [x] have fun :)
