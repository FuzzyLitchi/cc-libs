local movelib = require("movelib")
local timber = require("timber")("quarry")
local cliparser = require("cliparser")

local args = { ... }

-- TODO:
-- -- [ ] Let you pick which corner we start from (rn we assume top left corner)

function main()
    local cliOptions = {
        width = {
            short = "w",
            long = "width",
            description = "left/right from the perspective of the turtle",
            type = "int",
            required = true,
        },
        length = {
            short = "l",
            long = "length",
            description = "forward/backward from the perspective of the turtle",
            type = "int",
            required = true,
        },
        height = {
            short = "h",
            long = "height",
            description = "up/down from the perspective of the turtle",
            type = "int",
            required = true,
        },
    }

    local args = cliparser.parse(args, cliOptions)
    print(args.width)
    print(args.length)
    print(args.height)

    movelib.run("2R")
    do
        local has_block, block = turtle.inspect()
        if not has_block or not block.name == "minecraft:chest" then
            timber.error("Please place a chest behind me.")
            movelib.run("2R")
            return
        end
    end
    dumpInventory()
    movelib.run("2R")


    for depth = restoreProgress(), args.height, 3 do
        digToDepth(depth, args)
        saveProgress(depth)
    end

    -- Do the rest if it's not a multiple of 3
    if args.height % 3 ~= 0 then
        digToDepth(args.height, args)
        saveProgress(depth)
    end
end

function digToDepth(depth, args)
    assert(depth > 0)

    -- We start inside the digging area
    -- But the current y-level has to contain no blocks
    -- We dig 1 block down and then the rest of that xz plane.
    actionString = depth-1 .. "#D Md "
    actionString = actionString .. math.floor((args.width-1)/2) .. "(" .. args.length-1 .. "(#FMdMu) R#FMdMuR " .. args.length-1 .. "(#FMdMu) L#FMdMuL) "
    -- We're missing 1 or 2 rows, so we do them now
    if args.width % 2 == 0 then
        -- We're missing 2 rows
        actionString = actionString .. args.length-1 .."(#FMdMu) R#FMdMuR " .. args.length-1 .. "(#FMdMu) "
        -- Go home
        actionString = actionString .. depth-1 .. "U L" .. args.width-1 .."B R"
    else
        -- We're missing 1 row
        actionString = actionString .. args.length-1 .."(#FMdMu) "
        -- Go home
        actionString = actionString .. depth-1 .. "U" .. args.length-1 .."B R" .. args.width-1 .."B R"
    end
    print(actionString)

    actions = movelib.parse(movelib.tokenize(actionString))
    fuelRequired = movelib.calculateRequiredFuel(actions)

    if fuelRequired > turtle.getFuelLevel() then
        error("Fuel too low. Operation requires " .. fuelRequired .. " fuel.")
    end

    movelib.doActions(actions)

    dumpInventory()
    movelib.run("2R")
end

function restoreProgress()
    local file = fs.open("progress.depth", "r")
    if type(file) ~= "table" then
        return 3
    else
        return tonumber(file.readAll()) + 3
    end
    file.close()
end

function saveProgress(n)
    local file = fs.open("progress.depth", "w")
    file.write(tostring(n))
    file.close()
end

function dumpInventory()
    do
        local has_block, block = turtle.inspect()
        if not has_block or not block.name == "minecraft:chest" then
            error("WTF NO CHEST")
        end
    end

    for i = 1, 16 do
        turtle.select(i)
        turtle.drop()
    end
    turtle.select(1)
end


timber.startWithLogging(main)