local movelib = require("movelib")
local timber = require("timber")("tree")

local oak = {}
local spruce = {}

function main()
    timber.trace("Starting...")

    while true do
        approach()
        if spruce.isGrown() then
            spruce.fell()
            spruce.replant()
            sleep(120)
            bone_meal()
        end
        retreat()
        sleep(20)
        -- movelib.run("R")
    end
end

function bone_meal()
    for i = 1, 16 do
        local item = turtle.getItemDetail(i)
        if item and item.name == "minecraft:bone_meal" then
            usesLeft = item.count
            turtle.select(i)
            while usesLeft > 0 do
                turtle.place()

                local _, data = turtle.inspect()
                if data.name == "minecraft:spruce_log" then
                    return
                elseif data.name == "minecraft:spruce_sapling" then
                    -- continue
                else
                    error("There should be a sapling or log.")
                end
                usesLeft = usesLeft - 1
            end
        end
    end
end

-- Tree doesn't grow if we're too close
function approach()
    movelib.run("2F")
end

function retreat()
    movelib.run("2B")
end

function oak.replant()
    turtle.select(1) -- First slot
    local item = turtle.getItemDetail()
    if item and item.name ~= "minecraft:oak_sapling" then
        error("Please place oak saplings in slot 1")
    end

    -- Planting procedure
    movelib.run("P")
end

function oak.isGrown()
    local _, data = turtle.inspect()
    if data.name == "minecraft:oak_log" then
        return true
    elseif data.name == "minecraft:oak_sapling" then
        return false
    else
        error("There should be a sapling or log.")
    end
end

function oak.fell()
    -- Get in position
    movelib.run("MF")
    
    -- Go up and count the height of the tree
    local height = 0
    while true do
        height = height + 1
        movelib.run("MuU")
        local exists = turtle.inspectUp()
        if not exists then
            break
        end
    end

    timber.info("Just felled a tree " .. tostring(height) .. " high!")

    for _ = 1, height do
        movelib.run("D") -- Dig layer by layer
    end

    -- Move back
    movelib.run("B")
end

--    F    compas
--   L R
--    B
--
--   XX
--   XX
--    ^  turtle is the arrow
-- This function replants 4 saplings and assumes the marked area is cleared.
function spruce.replant()
    turtle.select(1) -- First slot
    local item = turtle.getItemDetail()
    if item and item.name ~= "minecraft:spruce_sapling" then
        error("Please place spruce saplings in slot 1")
        if item.count < 4 then
            error("Need at least 4 saplings")
        end
    end

    -- Planting procedure
    movelib.run("2F 2(LP RBP)")
end

-- Checks if the sapling has grown
-- Returns
function spruce.isGrown()
    local _, data = turtle.inspect()
    if data.name == "minecraft:spruce_log" then
        return true
    elseif data.name == "minecraft:spruce_sapling" then
        return false
    else
        return false
        --error("There should be a sapling or log.")
    end
end

function spruce.fell()
    -- Get in place
    movelib.run("MFM")

    -- Go up and count the height of the tree
    local height = 0
    while true do
        height = height + 1
        movelib.run("MuUM")
        local exists = turtle.inspectUp()
        if not exists then
            break
        end
    end

    -- Reposition turtle
    movelib.run("LMFRM") -- "L#FR M"

    for _ = 1, height do
        movelib.run("MdDM") -- Dig layer by layer
    end

    -- Reset position
    movelib.run("RFLB")
end

timber.startWithLogging(main)