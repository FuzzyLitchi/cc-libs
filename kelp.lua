--[[
This program crafts dried kelp into dried kelp blocks, and is supposed to
be the only program running on a turtle. It takes no fuel, and is fully
autonomous as long as it is started. It assumes a chest with dried kelp is
below it, and that a hopper (or some other container) needs to contain
kelp blocks above it.
]]--

local timber = require("timber")("kelp")

function main()
    checkInventory()
    cleanUpInventory()

    while true do
        checkInventory()
        craftKelp()
    end
end

function contains(array, value)
    for _, element in ipairs(array) do
        if value == element then
            return true
        end
    end
    
    return false
end

-- We check that our inventory only contains dried kelp and dried kelp blocks
function checkInventory()
    allowedItems = {
        "minecraft:dried_kelp",
        "minecraft:dried_kelp_block"
    }

    local valid = true

    for i = 1,16 do
        local item = turtle.getItemDetail(i)

        if item and not contains(allowedItems, item.name) then
            timber.error("Illegal item " .. item.name .. " found.")
            valid = false
        end
    end

    if not valid then
        error("Inventory polluted")
    end
end

-- If we have any kelp material on us, we put it back where it should be
function cleanUpInventory()
    for i = 1,16 do
        local item = turtle.getItemDetail(i)

        if item then
            if item.name == "minecraft:dried_kelp" then
                turtle.select(i)
                turtle.dropDown()
            elseif item.name == "minecraft:dried_kelp_block" then
                turtle.select(i)
                turtle.dropUp()
            end
        end
    end
end

function craftKelp()
    local chest = peripheral.find("minecraft:chest")

    local sum = 0
    for i, item in ipairs(chest.list()) do
        if item.name ~= "minecraft:dried_kelp" then
            error("I found not kelp WTF "..item.name)
        end
        sum = sum + item.count
    end

    -- If we have more than this much it fucks up our inventory
    if sum > 64*9 then
        sum = 64*9
    end

    -- we want to suck multiples of 9 so we can craft the kelp
    local kelpCount = sum - (sum%9)
    if kelpCount == 0 then
        sleep(10)
        return
    end
    
    do
        turtle.select(1)
        local suckAmount = kelpCount

        while suckAmount >= 64 do
            turtle.suckDown(64)
            suckAmount = suckAmount - 64
        end
        turtle.suckDown(suckAmount)
    end

    local kelpPerSlot = kelpCount/9

    local recipe = {kelpPerSlot, kelpPerSlot, kelpPerSlot, 0, kelpPerSlot, kelpPerSlot, kelpPerSlot, 0, kelpPerSlot, kelpPerSlot, kelpPerSlot, 0}

    for i = 1, 11 do
        local kelpForThisSlot = recipe[i]
        local item = turtle.getItemDetail(i)
        if item and item.count > kelpForThisSlot then
            turtle.select(i)

            local victimSlot = i + 1
            while turtle.getItemCount() > kelpForThisSlot do
                turtle.transferTo(victimSlot, turtle.getItemCount() - kelpForThisSlot)
                victimSlot = victimSlot + 1
            end
        elseif not item or item.count < kelpForThisSlot then
            error("TODO")
            
            -- turtle.select(i)

            -- local victimSlot = i + 1
            -- while turtle.itemCount() > kelpForThisSlot then
            --     turtle.transferTo(victimSlot, turtle.itemCount() - kelpForThisSlot)
            --     victimSlot = victimSlot + 1
            -- end
        end
    end
    -- Recipe done

    turtle.craft()
    turtle.dropUp()
end

timber.startWithLogging(main)
