-- TODO, integrate this into timber somehow.
LOG_CHANNEL = 108

function receive()
    local modem = peripheral.find("modem")
    modem.open(LOG_CHANNEL)

    local _, _, _, _, packet, distance = os.pullEvent("modem_message")
    -- local event, side, channel, replyChannel, message, distance;
    -- while true do
    --     event, side, channel, replyChannel, message, distance = os.pullEvent("modem_message")
    --     if channel ~= LOG_CHANNEL then
    --         os.queueEvent(event, side, channel, replyChannel, message, distance)
    --     end
    -- end
    local packet = textutils.unserialize(packet)
    if not (packet.time and packet.message and packet.machine) then
        error("Invalid packet")
    end

    packet.distance = distance

    return packet
end

function main()
    print("Started logging...")

    while true do
        local message = receive()

        local formatedTime = os.date("%H:%M:%S", message.time)
        print("["..formatedTime.. " "..message.machine.."]")

        if message.level == "error" then
            term.setTextColor(colors.red)
        elseif message.level == "warning" then
            term.setTextColor(colors.yellow)
        elseif message.level == "info" then
            term.setTextColor(colors.lightBlue)
        else
        -- elseif message.level == "trace" then
            term.setTextColor(colors.white)
        end

        print(message.message)
        term.setTextColor(colors.white)
    end
end

main()