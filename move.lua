local movelib = require("movelib")
local cliparser = require("cliparser")

local args = { ... }

function main(args)
    local ignoreFuel = false
    local actions
    
    local cliOptions = {
        ignoreFuel = {
            short = "f",
            long = "ignore-fuel",
            description = "Don't check fuel level before running actions.",
            type = "flag",
        },
        ignoreMissing = {
            short = "m",
            long = "ignore-missing",
            description = "Don't check if there's enough items before running actions.",
            type = "flag",
        },
        inverse = {
            short = "i",
            long = "inverse",
            description = "Take the inverse path of the one described.",
            type = "flag",
        },
        INPUT = {
            description = "The path for the turtle to take.",
            type = "string",
        },
    }

    local args = cliparser.parse(args, cliOptions)

    -- Check fuel
    local actions = movelib.parse(movelib.tokenize(args.INPUT))

    if args.inverse then
        actions = movelib.inverseActions(actions)
    end

    local fuelRequired = movelib.calculateRequiredFuel(actions)

    if fuelRequired > turtle.getFuelLevel() and not args.ignoreFuel then
        error("Fuel too low. Operation requires " .. fuelRequired .. " fuel.")
    end

    -- Check that we have items (if we're placing any down)
    -- TODO: count the amount of items needed
    -- if movelib.findInActions(actions, function(action) return action.action:sub(1,1) == "P" end) and not args.ignoreMissing then
    --     print("Placing is illegal!!!!!")
    --     -- assert(turtle.getItemCount() >= 1, "Turtle placing but no items.")
    -- end

    movelib.doActions(actions)
end

main(args)