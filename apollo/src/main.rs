use std::{
    env::args, fs, fs::OpenOptions, io::Write, path::PathBuf, sync::mpsc::channel, time::Duration,
};

use anyhow::{anyhow, bail, Result};
use notify::{watcher, DebouncedEvent, RecursiveMode, Watcher};
use tracing::{error, info, Level};
use tracing_subscriber::FmtSubscriber;

const CC_PATH: &str = r"C:\Users\Polly\Desktop\MultiMC\instances\CC\.minecraft\saves\World Domination!\computercraft\computer";

fn main() -> Result<()> {
    // a builder for `FmtSubscriber`.
    let subscriber = FmtSubscriber::builder()
        // all spans/events with a level higher than TRACE (e.g, debug, info, warn, etc.)
        // will be written to stdout.
        .with_max_level(Level::WARN)
        // completes the builder.
        .finish();

    tracing::subscriber::set_global_default(subscriber).expect("setting default subscriber failed");

    // Get the computer ids we're writing to
    let destinations_str: Vec<String> = args().skip(1).collect();

    // Check which ones exist
    let mut availabe: Vec<u64> = Vec::new();
    for file in fs::read_dir(CC_PATH)?.flatten() {
        let computer_id: u64 = file.file_name().into_string().unwrap().as_str().parse()?;
        availabe.push(computer_id);
    }

    // Check if the user input is sane
    let destinations: Vec<u64>;
    if destinations_str.is_empty() {
        bail!("No computer ids specified.")
    } else if destinations_str.len() == 1 && destinations_str[0] == "all" {
        destinations = availabe;
    } else {
        destinations = destinations_str
            .iter()
            .map(|s| s.parse::<u64>())
            .collect::<Result<_, _>>()?;
        for n in &destinations {
            if !availabe.contains(n) {
                bail!("Computer {} doesn't exist", n)
            }
        }
    }

    // Create a channel to receive the events.
    let (tx, rx) = channel();

    // Create a watcher object, delivering debounced events.
    // The notification back-end is selected based on the platform.
    let mut watcher = watcher(tx, Duration::from_secs(1)).unwrap();

    // Add a path to be watched. All files and directories at that path and
    // below will be monitored for changes.
    watcher.watch("..", RecursiveMode::Recursive).unwrap();

    loop {
        match rx.recv() {
            Err(e) => error!("watch error: {:?}", e),
            Ok(event) => match event {
                DebouncedEvent::Write(path) => {
                    if let Err(e) = handle_write(path, destinations.as_slice()) {
                        error!("Error handling write: {:?}", e);
                    }
                }
                _ => (),
            },
        }
    }
}

#[tracing::instrument]
fn handle_write(path: PathBuf, destinations: &[u64]) -> Result<()> {
    if path.extension().map(|s| s == "lua").unwrap_or(false) {
        // Get data
        let contents = fs::read(&path)?;

        for computer_id in destinations {
            // Write into new file
            let mut destination = PathBuf::from(CC_PATH);
            destination.push(computer_id.to_string());
            destination.push(
                path.file_name()
                    .ok_or_else(|| anyhow!("No file in path: {:?}", path))?,
            );
            info!("Writing to {:?}", &destination);

            // Open or create new file
            let mut file = OpenOptions::new()
                .truncate(true)
                .create(true)
                .write(true)
                .open(destination)?;
            file.write_all(&contents)?;
        }
    } else {
        info!("Not a lua file")
    }

    Ok(())
}
