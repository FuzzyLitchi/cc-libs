local movelib = require("movelib")
local pretty = require("cc.pretty")

local args = { ... }


function main()
    local context = {
        depth = 80,
        distanceToNextMineshaft = 0,
    }

    findNextMineshaft(pathToNextMineshaft(context.distanceToNextMineshaft), context)

    while true do
        local mineshaft = mineshaftActions(context.depth)
        movelib.doActions(mineshaft, {afterAction = placeTorch})

        -- We finished mining, check if we have enough inventory space
        if not enoughInventory(context) then
            -- We don't, empty it and go back
            local back = pathBack(context.distanceToNextMineshaft + 1)
            movelib.doActions(back)

            dumpInventory()
            
            context.distanceToNextMineshaft = context.distanceToNextMineshaft + 2
            print("Getting back out there")
            findNextMineshaft(pathToNextMineshaft(context.distanceToNextMineshaft), context)
        else
            -- Check if we have enough fuel
            local forward = movelib.parse(movelib.tokenize("LFL"))
            local back = pathBack(context.distanceToNextMineshaft + 3)
            local fuelRequired = movelib.calculateRequiredFuel(forward) + movelib.calculateRequiredFuel(mineshaft) + movelib.calculateRequiredFuel(back)

            -- Not enough fuel
            if fuelRequired > turtle.getFuelLevel() then
                print("Fuel too low. A minimum of " .. fuelRequired .. " fuel is required. Heading home")
                movelib.doActions(pathBack(context.distanceToNextMineshaft + 1))
                exit()
            end

            --Enough fuel
            movelib.doActions(forward)
        end
    end
    -- "MFMuMd" * depth + "R MFMuMd R" + (depth-1) * "MFMuMd
end

--- Forward is the path to the first mineshaft
function findNextMineshaft(forward, context)
    local mineshaft = mineshaftActions(context.depth)
    local back = pathBack(context.distanceToNextMineshaft + 1)
    
    local fuelRequired = movelib.calculateRequiredFuel(forward) + movelib.calculateRequiredFuel(mineshaft) + movelib.calculateRequiredFuel(back)

    if fuelRequired > turtle.getFuelLevel() then
        error("Fuel too low. A minimum of " .. fuelRequired .. " fuel is required.")
    end

    -- Move to next mineshaft
    movelib.doActions(forward)

    while true do
        -- Check if there is a block
        local exists = turtle.inspect()
        if exists then
            -- We found the mineshaft
            return
        else
            -- Check the next one
            context.distanceToNextMineshaft = context.distanceToNextMineshaft + 1
            local forward = movelib.parse(movelib.tokenize("RFL"))
            local mineshaft = mineshaftActions(context.depth)
            local back = pathBack(context.distanceToNextMineshaft + 1)

            -- Not enough fuel
            if fuelRequired > turtle.getFuelLevel() then
                print("Fuel too low. A minimum of " .. fuelRequired .. " fuel is required. Heading home")
                movelib.doActions(pathBack(context.distanceToNextMineshaft))
                exit()
            end

            -- Enough fuel
            movelib.doActions(forward)
        end
    end
end

function pathToNextMineshaft(distanceToNextMineshaft)
    local path = "UL" .. distanceToNextMineshaft .. "F L"
    return movelib.parse(movelib.tokenize(path))
end

function pathBack(distanceBack)
    local path = "R" .. distanceBack .. "F LD"
    return movelib.parse(movelib.tokenize(path))
end

function mineshaftActions(depth)
    local path = depth .. "(M#FMuMd)" .. "R M#FMuMd R" .. depth - 1 .. "(M#FMuMd) F"
    return movelib.parse(movelib.tokenize(path))
end

-- Let's say we want 3 empty slots and a total of 8*depth storage spaces
function enoughInventory(context)
    local emptySlots = 0
    local spaceLeft = 0

    for i = 1, 16 do
        if turtle.getItemCount(i) == 0 then
            emptySlots = emptySlots + 1
            spaceLeft = spaceLeft + 64
        else
            spaceLeft = spaceLeft + turtle.getItemSpace(i)
        end
    end

    return emptySlots >= 3 and spaceLeft >= context.depth * 8
end

function placeTorch(context)
    -- Only try to place torches after we mined down
    if context.lastAction.action == "Md" and torchPattern(context.position.x, context.position.z) then
        -- Find torch
        local torch = findTorch()
        if not torch then
            print("No torches!!")
            return
        end

        -- Place torch
        turtle.select(torch)
        turtle.placeDown()
    end
end

-- Returns whether or not a position is part of the torch pattern
function torchPattern(x, y)
    return (x % 12 == 0 and y % 14 == 0) or ((x+6) % 12 == 0 and (y+7) % 14 == 0)
end

function findTorch()
    for i = 1, 16 do
        local details = turtle.getItemDetail()
        if details.name == "minecraft:torch" then
            return i
        end
    end
end

function dumpInventory()
    for i = 1, 16 do
        turtle.select(i)
        local details = turtle.getItemDetail()
        if details and details.name ~= "minecraft:torch" then
            turtle.drop()
        end
    end
    turtle.select(1)
end

main()