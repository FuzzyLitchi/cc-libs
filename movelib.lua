-- A library for easier turtle movement

--- Movement actions
--- Forward:    F
--- Back:       B
--- Up:         U
--- Down:       D
--- Turn right: R
--- Turn left:  L

--- Place/Dig
--- P Pu Pd -- Place
--- M Du Md -- Mine/Dig

--- Modifiers are prepended
--- Force (dig if necessary): #
--- Numerical (repeat): 10
--- Infinity (repeats until terminate): *
--- Repeat until fail: ? -- can't be mixed with #

--- Groups
--- (FB)

-- Action
-- {
--    action = "forward" | group,
--    times = 1 | 10 | inf | math.hug 
-- }

-- # Examples
-- movelib.run("2F 2(LP RBP)")
-- 
-- Turtle moves 2 forward, and then repeats the following steps twice: Turn left, place, turn right, move back place.
-- This command replants a 2x2 square in front of the turtle.

local pretty = require("cc.pretty")

local movelib = {}

function movelib.tokenize(toParse)
    local tokens = {}

    while toParse:len() > 0 do
    while true do -- This second while loop makes break act as continue
        
        -- Check if number
        local number, rest = captureInteger(toParse)
        if number then
            table.insert(tokens, {number = number})
            toParse = rest
            break --continue
        end

        -- Check if group
        local group, rest = captureGroup(toParse)
        if group then
            table.insert(tokens, {group = movelib.tokenize(group:sub(2, -2))})
            toParse = rest
            break
        end

        -- Check two chars
        local twoChar = toParse:sub(1, 2)
        if contains({"Mu", "Md", "Pu", "Pd"}, twoChar) then
            table.insert(tokens, {action = twoChar})
            toParse = toParse:sub(3)
            break
        end

        -- Check one char
        local char = toParse:sub(1, 1)

        -- Modifiers
        if contains({"#", "*", "?"}, char) then
            table.insert(tokens, {modifier = char})
            toParse = toParse:sub(2)
            break
        end

        -- Actions
        if contains({"F", "B", "U", "D", "R", "L", "M", "P"}, char) then
            table.insert(tokens, {action = char})
            toParse = toParse:sub(2)
            break
        end

        -- Ignore whitespace
        if contains({" ", "\t", "\n", "\r"}, char) then
            toParse = toParse:sub(2)
            break
        end

        -- Handle ()
        error("unkown character: " .. char)

        break -- Means continue
    end
    end

    return tokens
end

local MULTIPLE_TIMES_ERROR = 'An action can only be have one or zero of these: "*", "?" or a number.'
local MULTIPLE_FORCE_ERROR = 'An action can only be have one or zero "#".'
function movelib.parse(tokens)
    local actions = {}
    local next = 1

    while next <= #tokens do
        local action = {
            action = nil,
            times = nil,
            force = false,
        }

        while true do
            local token = tokens[next]
            next = next + 1

            assert(token, "Expected token found EOF")

            if token.modifier then
                if token.modifier == "#" then
                    assert(not action.force, MULTIPLE_FORCE_ERROR)
                    action.force = true
                elseif token.modifier == "*" then
                    assert(not action.times, MULTIPLE_TIMES_ERROR)
                    action.times = math.huge
                elseif token.modifier == "?" then
                    assert(not action.times, MULTIPLE_TIMES_ERROR)
                    error("Whoops not implemented lol")
                end
            elseif token.number then
                assert(not action.times, MULTIPLE_TIMES_ERROR)
                action.times = token.number
            elseif token.action then
                -- Finishes action
                action.action = token.action
                break
            elseif token.group then
                action.action = movelib.parse(token.group)
                break
            end
        end

        table.insert(actions, finalizeAction(action))
    end

    return actions
end

function finalizeAction(action)
    action.times = action.times or 1

    -- Check that it's valid
    if action.force then
        assert(contains({"F", "U", "D"}, action.action), "You can only force these operations: F, U, D")
    end

    return action
end

-- returns integer and rest
function captureInteger(data)
    local len = 0
    while isInteger(data:sub(len + 1, len + 1)) do
        len = len + 1
    end

    if len == 0 then
        return nil, data
    else
        return tonumber(data:sub(1, len)), data:sub(len + 1)
    end
end

function isInteger(char)
    assert(char:len() <= 1, "isInteger() expects only chars aka strings of length 1")
    return contains({"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"}, char)
end

-- returns outer group and rest
function captureGroup(data)
    if data:sub(1, 1) ~= "(" then
        return nil, data
    end
    
    local len = 1
    local bracketsDeep = 1
    while true do
        local char = data:sub(len + 1, len + 1)
        len = len + 1

        if char == ")" then
            bracketsDeep = bracketsDeep - 1
        elseif char == "(" then
            bracketsDeep = bracketsDeep + 1
        end

        if bracketsDeep == 0 then
            break
        end
    end

    return data:sub(1, len), data:sub(len + 1)
end

function movelib.calculateRequiredFuel(actions)
    local totalFuel = 0
    for _, action in ipairs(actions) do
        if type(action.action) == "table" then
            totalFuel = totalFuel + action.times * movelib.calculateRequiredFuel(action.action)
        elseif contains({"R", "L", "M", "Mu", "Md", "P", "Pu", "Pd"}, action.action) then
            -- Uses no fuel
        elseif action.times == math.huge then
            -- If any action that isn't a rotation should go on for infinity, we conclude that it must use infinite fuel
            return math.huge
        else
            totalFuel = totalFuel + action.times
        end
    end

    return totalFuel
end

--- Returns the inverse of the current path.
--- In other words, if you took the path `actions` and then the path this function returns
--- you'd end up in the same place.
function movelib.inverseActions(actions)
    local inverseActions = {}

    -- Iterate from the end. So "FRU" becomes "DLB"
    for i = #actions, 1, -1 do
        local action = actions[i]

        local inverseAction = movelib.inverseAction(action)
        if inverseAction ~= nil then
            table.insert(inverseActions, inverseAction)
        end
    end

    return inverseActions
end

function movelib.inverseAction(action)
    local inverseAction = { times = action.times}

    if type(action.action) == "table" then
        print("group", action.action)
        inverseAction.action = movelib.inverseActions(action.action)
    else
        local actionIdentifier = _inverseAction(action)
        if actionIdentifier == nil then
            -- For now, we simply ignore the ones that have no inverse
            return nil
        end
        inverseAction.action = actionIdentifier
    end

    return inverseAction
end

-- This function just returns the action identifier, we have to construct an action "object".
function _inverseAction(action)
    -- Regular action
    if action.action == "F" then
        return "B"
    elseif action.action == "B" then
        return "F"
    elseif action.action == "U" then
        return "D"
    elseif action.action == "D" then
        return "U"
    elseif action.action == "R" then
        return "L"
    elseif action.action == "L" then
        return "R"
    elseif action.action == "M" then
        -- TODO: What should we return?
        print("No inverse for mining.")
        return nil;
    elseif action.action == "Mu" then
        print("No inverse for mining.")
        return nil;
    elseif action.action == "Md" then
        print("No inverse for mining.")
        return nil;
    elseif action.action == "P" then
        print("No inverse for placing (rn at least).")
        return nil;
    elseif action.action == "Pu" then
        print("No inverse for placing (rn at least).")
        return nil;
    elseif action.action == "Pd" then
        print("No inverse for placing (rn at least).")
        return nil;
    end

    print("Action "..tostring(action).." not implemented.")
end

--- Executes a parsed set of action
function movelib.doActions(actions, context)
    context = context or {}

    for _, action in ipairs(actions) do
        for _ = 1, action.times do
            if type(action.action) == "table" then
                movelib.doActions(action.action, context)
            else
                local didAction, reason = doAction(action)

                -- Handle failed actions
                if not didAction and not ignoreFailure(action.action) then
                    error("Failed to do action \"" .. action.action .. "\":" .. reason)
                end

                if context.afterAction then
                    context.afterAction({lastAction = action, position = movelib.position})
                end
            end
        end
    end
end

--- Ignores failures for some actions, specifically mining.
function ignoreFailure(action)
    return contains({"M", "Mu", "Md"}, action)
end

--- Internal 
function doAction(action)
    -- Force action
    if action.force then
        if action.action == "F" then
            return movelib.forceForward()
        elseif action.action == "U" then
            error("Not implemented")
        elseif action.action == "D" then
            return movelib.forceDown()
        end
    end

    -- Regular action
    if action.action == "F" then
        return movelib.forward()
    elseif action.action == "B" then
        return movelib.back()
    elseif action.action == "U" then
        return movelib.up()
    elseif action.action == "D" then
        return movelib.down()
    elseif action.action == "R" then
        return movelib.turnRight()
    elseif action.action == "L" then
        return movelib.turnLeft()
    elseif action.action == "M" then
        return turtle.dig()
    elseif action.action == "Mu" then
        return turtle.digUp()
    elseif action.action == "Md" then
        return turtle.digDown()
    elseif action.action == "P" then
        return turtle.place()
    elseif action.action == "Pu" then
        return turtle.placeUp()
    elseif action.action == "Pd" then
        return turtle.placeDown()
    end

    error("\"" .. action .. "\" is not a valid action")
end

-- These blocks can't be broken, but also don't prevent us from advancing
local IGNORED_BLOCKS = {"minecraft:water", "minecraft:lava"}

function movelib.forceForward()
    while true do
        local exists, details = turtle.inspect()
        -- Clear path
        if not exists or contains(IGNORED_BLOCKS, details.name) then
            break
        end

        -- There's a block
        turtle.dig()
    end
    return movelib.forward()
end

function movelib.forceDown()
    while true do
        local exists, details = turtle.inspectDown()
        -- Clear path
        if not exists or contains(IGNORED_BLOCKS, details.name) then
            break
        end

        -- There's a block
        turtle.digDown()
    end
    return movelib.down()
end

function movelib.run(actionText, context)
    movelib.doActions(movelib.parse(movelib.tokenize(actionText)), context)
end

function contains(array, value)
    for _, element in ipairs(array) do
        if value == element then
            return true
        end
    end
    
    return false
end

--- Returns the action where predicate(action) is true
--- Example:
--- movelib.findInActions(actions, function(other) return other.action == "F" end)
-- FIXME: infinite recursion (maybe there's a cycle in actions)
function movelib.findInActions(actions, predicate)
    print(actions)

    for _, action in ipairs(actions) do
        if type(action.action) == "table" then
            -- Recurse
            local maybe = movelib.findInActions(action.action, predicate)
            -- If we found it, return
            if maybe then
                return maybe
            end
        else
            -- If we found it, return
            if predicate(action) then
                return action
            end
        end
    end

    return false
end

--- Position
--- x, y and z are signed integers
--- facing is either "north", "east", "south" or "west"
--- east is positive X, south is positive Z
movelib.position = {
    x = 0,
    y = 0,
    z = 0,
    facing = "north",
}

function movelib.forward()
    local success, error = turtle.forward()
    if success then
        -- Update position
        if movelib.position.facing == "east" then
            movelib.position.x = movelib.position.x + 1
        elseif movelib.position.facing == "west" then
            movelib.position.x = movelib.position.x - 1
        elseif movelib.position.facing == "south" then
            movelib.position.z = movelib.position.z + 1
        elseif movelib.position.facing == "north" then
            movelib.position.z = movelib.position.z - 1
        end
    end
    return success, error
end

function movelib.back()
    local success, error = turtle.back()
    if success then
        -- Update position
        if movelib.position.facing == "east" then
            movelib.position.x = movelib.position.x - 1
        elseif movelib.position.facing == "west" then
            movelib.position.x = movelib.position.x + 1
        elseif movelib.position.facing == "south" then
            movelib.position.z = movelib.position.z - 1
        elseif movelib.position.facing == "north" then
            movelib.position.z = movelib.position.z + 1
        end
    end
    return success, error
end

function movelib.up()
    local success, error = turtle.up()
    if success then
        -- Update position
        movelib.position.y = movelib.position.y + 1
    end
    return success, error
end

function movelib.down()
    local success, error = turtle.down()
    if success then
        -- Update position
        movelib.position.y = movelib.position.y - 1
    end
    return success, error
end

function movelib.turnRight()
    local success, error = turtle.turnRight()
    if success then
        -- Update position
        if movelib.position.facing == "north" then
            movelib.position.facing = "east"
        elseif movelib.position.facing == "east" then
            movelib.position.facing = "south"
        elseif movelib.position.facing == "south" then
            movelib.position.facing = "west"
        elseif movelib.position.facing == "west" then
            movelib.position.facing = "north"
        end
    end
    return success, error
end

function movelib.turnLeft()
    local success, error = turtle.turnLeft()
    if success then
        -- Update position
        if movelib.position.facing == "north" then
            movelib.position.facing = "west"
        elseif movelib.position.facing == "east" then
            movelib.position.facing = "north"
        elseif movelib.position.facing == "south" then
            movelib.position.facing = "east"
        elseif movelib.position.facing == "west" then
            movelib.position.facing = "south"
        end
    end
    return success, error
end


return movelib